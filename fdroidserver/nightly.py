#!/usr/bin/env python3
#
# nightly.py - part of the FDroid server tools
# Copyright (C) 2014 Daniel Martí <mvdan@mvdan.cc>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import hashlib
import os
import paramiko
import subprocess
import sys
import tempfile
from argparse import ArgumentParser

from . import _
from . import common


options = None


def main():

    parser = ArgumentParser(usage="%(prog)s")
    common.setup_global_opts(parser)
    parser.add_argument("--show-secret-var", action="store_true", default=False,
                        help=_("Print the secret variable to the terminal for easy copy/paste"))
    parser.add_argument("--file", default='app/build/outputs/apk/*.apk',
                        help=_('The the file to be included in the repo (path or glob)'))
    options = parser.parse_args()
    common.read_config(None)

    if 'CI' in os.environ:
        v = os.getenv('DEBUG_KEYSTORE')
        debug_keystore = None
        if v:
            debug_keystore = base64.b64decode(v)
        if not debug_keystore:
            print(_('DEBUG_KEYSTORE is not set or the value is incomplete'))
            sys.exit(1)
        androiddir = os.path.join(os.getenv('HOME'), '.android')
        os.makedirs(androiddir, exist_ok=True)
        keystore = os.path.join(androiddir, 'debug.keystore')
        if os.path.exists(keystore):
            print(_('WARNING: overwriting existing {path}').format(path=keystore))
        with open(keystore, 'wb') as fp:
            fp.write(debug_keystore)

        repo_basedir = os.path.join(os.getcwd(), 'fdroid')
        os.makedirs(repo_basedir, exist_ok=True)
        os.chdir(repo_basedir)

        if 'CI_PROJECT_NAMESPACE' in os.environ and 'CI_PROJECT_NAME' in os.environ:
            # we are in GitLab CI
            repo_git_base = os.getenv('CI_PROJECT_NAMESPACE') + '/' + os.getenv('CI_PROJECT_NAME') + '-nightly'
            repo_base = 'https://gitlab.com/' + repo_git_base + '/raw/master/fdroid'
            servergitmirrors = 'git@gitlab.com:' + repo_git_base
        elif 'TRAVIS_REPO_SLUG' in os.environ:
            # we are in Travis CI
            repo_git_base = os.getenv('TRAVIS_REPO_SLUG') + '-nightly'
            _branch = os.getenv('TRAVIS_BRANCH')
            repo_base = 'https://raw.githubusercontent.com/' + repo_git_base + '/' + _branch
            servergitmirrors = 'git@github.com:' + repo_git_base
        else:
            print(_('ERROR: unsupported CI type, patches welcome!'))
            sys.exit(1)

        repobase = tempfile.mkdtemp(prefix='fdroid', dir=os.getcwd())
        os.chdir(repobase)
        config = ''
        config += "repo_name = '%s'\n".format(repo_git_base)
        config += "repo_url = '%s'\n".format(repo_base + '/repo')
        config += "archive_name = '%s'\n".format(repo_git_base + ' archive')
        config += "archive_url = '%s'\n".format(repo_base + '/archive')
        config += "servergitmirrors = '%s'\n".format(servergitmirrors)
        config += "keystore = '%s'\n".format(keystore)
        with open('config.py', 'w', mode=0o600) as fp:
            fp.write(config)

        # TODO generate qr code
        # TODO set qr.png as repo_icon and archive_icon
        # TODO find and copy APK
        # TODO run `fdroid update --rename-apks`
    else:
        keypass = 'android'
        keyalias = 'androiddebugkey'
        keystore = os.path.join(os.getenv('HOME'), '.android', 'debug.keystore')
        umask = os.umask(0o077)
        tmp_dir = tempfile.mkdtemp(prefix='.')
        print('tmp_dir', tmp_dir)
        privkey = os.path.join(tmp_dir, '.privkey')
        key_pem = os.path.join(tmp_dir, '.key.pem')
        p12 = os.path.join(tmp_dir, '.keystore.p12')
        subprocess.check_call([common.config['keytool'], '-importkeystore',
                               '-srckeystore', keystore, '-srcalias', keyalias,
                               '-srcstorepass', keypass, '-srckeypass', keypass,
                               '-destkeystore', p12, '-destalias', keyalias,
                               '-deststorepass', keypass, '-destkeypass', keypass,
                               '-deststoretype', 'PKCS12'])
        subprocess.check_call(['openssl', 'pkcs12', '-in', p12, '-out', key_pem,
                               '-passin', 'pass:' + keypass, '-passout', 'pass:' + keypass])
        subprocess.check_call(['openssl', 'rsa', '-in', key_pem, '-out', privkey,
                               '-passin', 'pass:' + keypass])
        os.remove(key_pem)
        os.remove(p12)
        os.chmod(privkey, 0o600)  # os.umask() should cover this, but just in case
        ssh_dir = os.path.join(os.getenv('HOME'), '.ssh')
        os.makedirs(os.path.dirname(ssh_dir), exist_ok=True)

        rsakey = paramiko.RSAKey.from_private_key_file(privkey)
        fingerprint = base64.b64encode(hashlib.sha256(rsakey.asbytes()).digest()).decode('ascii').rstrip('=')
        ssh_private_key = os.path.join(ssh_dir, 'debug_keystore_' + fingerprint + '_id_rsa')
        os.rename(privkey, ssh_private_key)

        os.umask(umask)
        pub = rsakey.get_name() + ' ' + rsakey.get_base64() + ' ' + ssh_private_key
        with open(ssh_private_key + '.pub', 'w') as fp:
            fp.write(pub)
        with open(keystore, 'rb') as fp:
            debug_keystore = base64.standard_b64encode(fp.read()).decode('ascii')
        print(_('\nSSH Public Key to be used as Deploy Key:'))
        print(pub)
        if options.show_secret_var:
            print(_('\n{path} encoded for the DEBUG_KEYSTORE secret variable:')
                  .format(path=keystore))
            print(debug_keystore)


if __name__ == "__main__":
    main()
